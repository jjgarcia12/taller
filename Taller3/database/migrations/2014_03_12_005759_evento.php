<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Evento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lugar_choque');
            $table->string('placa_vehiculo');
            $table->string('foto');
            $table->string('numero_departe');
            $table->string('numero_deregistro');
            $table->string('estado');
             $table->timestamps();
        });




        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('evento'); 


           }
}
