<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;

use Closure;

class IsAdmin
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->user()->tipo_perfil != 'admin') {
            ///$this->auth->logout();
            ///if ($request->ajax()) {
            return redirect()->to('home');
                ///return view('home');
            } else {
                return $next($request);
               /// return redirect()->to('auth/login');
            }
        }

        
    



}
