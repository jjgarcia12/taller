<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\evento;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class eventos extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
        $evento = evento::paginate(5);




        return view('evento.index',compact('evento'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }


        public function create()
    {


        $evento = evento::all();

        return view('evento.create',compact('evento'));

    }
    public function store(Request $request)
    {

			$this->validate($request, [
            'lugar_choque' => 'required',
            'placa_vehiculo' => 'required',
            'foto' => 'required',
           
            'estado' => 'required',
            
        ]);

        evento::create($request->all());
     


        return redirect()->route('evento.index')
                        ->with('success','evento creado correctamente');
    }
    public function show($id)
    {

        $evento = evento::find($id);
        
        return view('evento.show',compact('evento'));
    }



 public function edit($id)
    {
        $evento = evento::find($id);
        return view('evento.edit',compact('evento'));    
    }



       public function update(Request $request, $id)
    {
        $this->validate($request, [
           
               'lugar_choque' => 'required',
            'placa_vehiculo' => 'required',
            'foto' => 'required',
            'numero_de parte' => 'required',
            'numero_de registro' => 'required',
            'estado' => 'required',
         
        ]);

        evento::find($id)->update($request->all());
        return redirect()->route('evento.index')
                        ->with('success','evento actualizada correctamente');
    }

       public function destroy($id)
    {
        evento::find($id)->delete();
        return redirect()->route('evento.index')
                        ->with('success','evento eliminada correctamente');
    }




}
