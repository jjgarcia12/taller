<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\eventos;
use App\User;


use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class evento extends Controller
{
   


    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
      
          $user = Auth::user();


   


        if($user->tipo_perfil=='Oficial de Transito'){



            $eventos = DB::table('eventos')

            ->where('numero_departe','')
                ->where('numero_deregistro','')
          ->paginate(5);


        return view('evento.index',compact('eventos'))
        ->with('i', (request()->input('page', 1) - 1) * 5);

}elseif ($user->tipo_perfil=='Oficial de Juzgado') {
    

            $eventos = DB::table('eventos')

            
            ->where('numero_deregistro','')



          ->paginate(5);


        return view('evento.index',compact('eventos'))
        ->with('i', (request()->input('page', 1) - 1) * 5);



}else{


    $eventos = DB::table('eventos')

            
                ->wherenull('numero_deregistro','')
          ->paginate(5);


        return view('evento.index',compact('eventos'))
        ->with('i', (request()->input('page', 1) - 1) * 5);




}






    }


        public function create()
    {


        $eventos = eventos::all();

               $user = Auth::user();


   


        if($user->tipo_perfil=='Usuario'){

            $eventos = eventos::all();

        return view('evento.create',compact('eventos'));

        }




       


        else{

 $eventos = eventos::paginate(5);

         
        return view('evento.index',compact('eventos'))
        ->with('i', (request()->input('page', 1) - 1) * 5);


}
    }


















    public function store(Request $request)
    {

			$this->validate($request, [
            'lugar_choque' => 'required',
            'placa_vehiculo' => 'required',
            'foto' => 'required',
         
            
        ]);

        eventos::create($request->all());
     


        return redirect()->route('evento.index')
                        ->with('success','evento creado correctamente');
    }
    public function show($id)
    {

        $eventos = eventos::find($id);
        
        return view('evento.show',compact('eventos'));
    }



 public function edit($id)
    {



         $user = Auth::user();


        $user->name;



   $evento = eventos::find($id);




        if($user->tipo_perfil=='Oficial de Transito'){

           $evento = eventos::find($id);

        return view('evento.editoficial',compact('evento'));



        }




       


        elseif($user->tipo_perfil=='Oficial de Juzgado'&&$evento->numero_departe!=''){
                 $evento = eventos::find($id);

        return view('evento.editojuzgado',compact('evento'));


                }else{






  return redirect()->route('evento.index');



        }    
    }



       public function update(Request $request, $id)
    {
        $this->validate($request, [
           
          
         
        ]);

        eventos::find($id)->update($request->all());
        return redirect()->route('evento.index')
                        ->with('success','evento actualizada correctamente');
    }

       public function destroy($id)
    {
        eventos::find($id)->delete();
        return redirect()->route('evento.index')
                        ->with('success','evento eliminada correctamente');
    }


}
