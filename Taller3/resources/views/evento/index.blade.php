@extends('home')

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Eventos</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('evento.create') }}">Eventos</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>lugar</th>
            <th>placa</th>
            <th>foto</th>
            <th>numero parte</th>
            <th>numero registro</th>
            <th>Estado</th>

            <th width="280px">Action</th>
        </tr>
            @foreach ($eventos as $evento)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $evento->lugar_choque}}</td>
        <td>{{ $evento->placa_vehiculo}}</td>
        <td>{{ $evento->foto}}</td>
       <td>{{ $evento->numero_departe}}</td>

       <td>{{ $evento->numero_deregistro}}</td>

       <td>{{ $evento->estado}}</td>
       
        <td>



        

        <a class="btn btn-primary" href="{{ route('evento.edit',$evento->id) }}">Crear Evento</a>





            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

 {!! $eventos->links() !!}
 
@endsection