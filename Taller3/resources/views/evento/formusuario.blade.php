<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre:</strong>
            {!! Form::text('lugar_choque', null, array('placeholder' => 'lugar_choque','class' => 'form-control')) !!}
            @if ($errors->has('lugar_choque'))
                <span class="help-block">
                    <strong>{{ $errors->first('lugar_choque') }}</strong>
                </span>
            @endif
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>placa_vehiculo:</strong>
            {!! Form::text('placa_vehiculo', null, array('placeholder' => 'placa_vehiculo','class' => 'form-control')) !!}
            @if ($errors->has('placa_vehiculo'))
                <span class="help-block">
                    <strong>{{ $errors->first('placa_vehiculo') }}</strong>
                </span>
            @endif
        </div>
    </div>

    
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>foto:</strong>
            {!! Form::text('foto', null, array('placeholder' => 'foto','class' => 'form-control')) !!}
            @if ($errors->has('foto'))
                <span class="help-block">
                    <strong>{{ $errors->first('foto') }}</strong>
                </span>
            @endif
        </div>
    </div>





    
        
            {!! Form::hidden('estado','abierto') !!}
        
     
  


   
    {!! Form::hidden('user_id', Auth::id() )!!}
    
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>