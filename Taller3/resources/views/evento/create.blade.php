@extends('home')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Crear evento</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('evento.index') }}"> Volver</a>
            </div>
        </div>
    </div>
   
    {!! Form::open(array('route' => 'evento.store','method'=>'POST')) !!}
         @include('evento.formusuario')
    {!! Form::close() !!}
@endsection